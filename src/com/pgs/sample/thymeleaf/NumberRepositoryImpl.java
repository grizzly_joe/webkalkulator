package com.pgs.sample.thymeleaf;

public class NumberRepositoryImpl implements NumberRepository {
	public double firstNumber;
	public double secondNumber;
	public char operation;

	public char getOperation() {
		return operation;
	}

	public double getResult() {
		return result;
	}

	public void setOperation(char operation) {
		this.operation = operation;
	}

	public void setResult(double result) {
		this.result = result;
	}

	public double result;

	public double getFirstNumber() {
		return firstNumber;
	}

	public double getSecondNumber() {
		return secondNumber;
	}

	public void setFirstNumber(double firstNumber) {
		this.firstNumber = firstNumber;
	}

	public void setSecondNumber(double secondNumber) {
		this.secondNumber = secondNumber;
	}

	@Override
	public double add(double firstValue, double secondValue) {
		return firstValue + secondValue;
	}

	@Override
	public double mult(double firstValue, double secondValue) {
		return firstValue * secondValue;
	}

	@Override
	public double div(double firstValue, double secondValue) {
		return firstValue / secondValue;
	}

	@Override
	public double sub(double firstValue, double secondValue) {
		return firstValue - secondValue;
	}
	
	@Override
	public double chooseOperation(char operation, double firstValue, double secondValue) {

		switch(operation) {
		
			case '+': return add(firstValue, secondValue);
			
			case '-': return sub(firstValue, secondValue);
			
			case '*': return mult(firstValue, secondValue); 
				
			case '/': return div(firstValue, secondValue);
			
			default: return 0;
				
		}
		
	}

}