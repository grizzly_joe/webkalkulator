package com.pgs.sample.thymeleaf;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pgs.sample.CalculatorController;
import com.pgs.sample.OperationResult;

@Controller
public class ThymeleafController {
	

	@RequestMapping(value = "/thymeleaf", method = RequestMethod.GET)
	public String calcForm(Model model) {
		model.addAttribute("numbers", new NumberRepositoryImpl());
		return "index";
	}

	@RequestMapping(value = "/thymeleaf", method = RequestMethod.POST)
	public String calcSubmit(@ModelAttribute NumberRepositoryImpl numberRepositoryImpl, Model model) {
		model.addAttribute("numberRepositoryImpl", numberRepositoryImpl);
		numberRepositoryImpl.result = numberRepositoryImpl.chooseOperation(
				numberRepositoryImpl.operation, numberRepositoryImpl.firstNumber, numberRepositoryImpl.secondNumber);
		
		return "result";
	}

}
