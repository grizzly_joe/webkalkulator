package com.pgs.sample.thymeleaf;

public interface NumberRepository {

	public double add(double firstValue, double secondValue);

	public double mult(double firstValue, double secondValue);

	public double div(double firstValue, double secondValue);

	public double sub(double firstValue, double secondValue);
	
	public double chooseOperation(char operation, double firstValue, double secondValue);
	
	

}
